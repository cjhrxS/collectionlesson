package second;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CharacterCounter {
	public static void main(String[] args) {
		String sample = "aaab bc";
		Map<String, Integer> mapka = new HashMap<String, Integer>();
		String[] array = getStringAsArray(sample);
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			for(int j = 0; j < array.length; j++) {
				if(array[i].equals(array[j])) {
					++count;
					mapka.put(array[i], count);
				} 
				if(j == array.length - 1){
					count = 0;
				}
			}
			}
		
		System.out.println(mapka.entrySet());
	}
	
	public static String[] getStringAsArray(String str) {
		String withOutSpace = str.replace(" ", "");
		String [] array = withOutSpace.split("(?!^)");
		return array;
	}
	
	
}
