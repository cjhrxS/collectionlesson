package annotation.differentFields;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class LoopWithReferences {
	  @CoolAnnotationWichWeCanUseWherever(name = "field of Class")
      private int field;
	  
	  public static void main(String[] args) {
		  @CoolAnnotationWichWeCanUseWherever(name = "local variable")
		  String localV = "someV";
		  
		  for (@CoolAnnotationWichWeCanUseWherever(name = "loop") int i = 0; i < args.length; i++) {
			//some code
		}
		  
		  try(@CoolAnnotationWichWeCanUseWherever(name="resource vaiable") BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			  
		  } catch (@CoolAnnotationWichWeCanUseWherever(name = "exception") Exception e) {
			//resource variable = its variable which implements AutoCloseable interface
		}
		  
		  float number = 1.6F;
		  long cast = (@CoolAnnotationWichWeCanUseWherever(name = "Cast") long) number;
		  
		  LoopWithReferences lp = new @CoolAnnotationWichWeCanUseWherever(name = "reference") LoopWithReferences();
	}
      
}
