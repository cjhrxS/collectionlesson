package annotation.annotateclass;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class App {
	
public static void main(String[] args) {
	CustomC c = new CustomC();
	System.out.println(checkAnnotation(c));
}



public static boolean checkAnnotation(Object o) {
	Class<?> c = o.getClass();  
	if(c.isAnnotationPresent(MyAnnotation.class)) {
		return true;
	}
	return false;
}
}
