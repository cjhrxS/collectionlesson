package fistTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomElements {

	
public static void main(String[] args) {
		
		List<Integer> array = getStreamOfRandomIntsWithRange(10, 1, 10);
		
//		array.add(10); //10 + 12 + 7 + 23
//		array.add(3);
//		array.add(12);
//		array.add(8);
//		array.add(7);
//		array.add(9);
//		array.add(23);
		
		
		
		List<Integer> pairList = getEvenNumberIfNumberLessThenMaxNumberValue(array, 5);
		
	     int sum = getSum(pairList);
	     System.out.println(sum);
		
		
		
	}
	
	public static List<Integer> getEvenNumberIfNumberLessThenMaxNumberValue(List<Integer> list, int maxNumberValue) {
		List<Integer> pairNumber = new ArrayList<Integer>();
		for (Integer element : list) {
		 Integer index = list.indexOf(element);
		  if((index % 2) == 0 && element <= maxNumberValue) {
			 pairNumber.add(element);
		  }
		}
		return pairNumber;
	}
	
	public static int getSum(List<Integer> array) {
    	return array.stream().mapToInt(element -> element).sum();
    }

	
	public static List<Integer> getStreamOfRandomIntsWithRange(int num, int min, int max) {
	    Random random = new Random();
	   return random.ints(num, min, max).sorted().boxed().collect(Collectors.toList());
	}
	
}
