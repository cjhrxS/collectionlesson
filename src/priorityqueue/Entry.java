package priorityqueue;

import java.util.Comparator;

public class Entry{

	private Integer value;
	private Entry nextObject;
	private int index;
	
    public Entry() {}
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Entry getNextObject() {
		return nextObject;
	}

	public void setNextObject(Entry nextObject) {
		this.nextObject = nextObject;
	}

	@Override
	public String toString() {
		return "Entry [value=" + value + ", nextObject=" + nextObject + ", index=" + index + "]";
	}

}
