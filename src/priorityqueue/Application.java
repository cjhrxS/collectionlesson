package priorityqueue;

public class Application {

	public static void main(String[] args) {
		
		ProorityQueue line = new ProorityQueue();
		line.insert(9);
		line.insert(2);
		line.insert(1);
		line.insert(5);
		line.insert(6);
		line.insert(-5);
		
		System.out.println(line);
		
		System.out.println(line.printValues());
		
		line.deleteByIndex(6);
		
		System.out.println(line.printValues());
		
		System.out.println(line);
		System.out.println("Method peek " + line.peek());
		
		line.poll();
		
		System.out.println(line);
		System.out.println(line.printValues());
		
		
		
	}
}
