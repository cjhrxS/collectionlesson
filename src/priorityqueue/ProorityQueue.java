package priorityqueue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import priorityqueue.Entry;;

public class ProorityQueue {
	
	
	private Entry head;
	private Entry tail;
	private static int index;

	public ProorityQueue() {
		this.head = null;
		this.tail = null;
		this.index = 0;
	}

	public void insert(Integer value) {
		Entry last = this.getTail();
		if(head == null) {
			Entry e = new Entry();
			e.setValue(value);
			e.setIndex(++index);
			head = e;
		} else if(head != null && tail == null){
			Entry newEntry = new Entry();
			newEntry.setValue(value);
			newEntry.setIndex(++index);
			if(newEntry.getValue() < this.getHead().getValue()) {
				head.setNextObject(newEntry);
				tail = newEntry;
			}else {
				tail = head;
			    head = newEntry;
			    head.setNextObject(tail);
			}
		} else {
			Entry privioslyHead = this.getHead();
			Entry privioslyTail = this.getTail();
			Entry newEntry = new Entry();
			newEntry.setValue(value);
			newEntry.setIndex(++index);
			
			Entry morePriority = findMorePriorityElement(newEntry);
			Entry lessPriority = findLessPriorityElement(newEntry);
			
			if(morePriority.equals(newEntry)) {
				head = morePriority;
				head.setNextObject(privioslyHead);
			}else if(lessPriority.equals(newEntry) && lessPriority.getValue() < privioslyTail.getValue()) {
				tail = lessPriority;
				privioslyTail.setNextObject(tail);
				
			}else {
				newEntry.setNextObject(next(morePriority));
      			morePriority.setNextObject(newEntry);
      			updateIndex();
				
			}
			
		}
	}

	public Entry getByIndex(int value) {
		Entry current = this.getHead();
	while(hasNext(current)) {
			Entry ne = next(current);
			current = ne;
			if(current.getIndex() == value) {
				return current;
			}
	
	}
		if(this.getHead().getIndex() == value) {
			current = this.getHead();
		};
		return current;
	}
	
	private Entry findMorePriorityElement(Entry entry) {
		Entry current = this.getHead();
		Entry entryWithPriority = null;
		if(hasNext(current)) {
		while(hasNext(current)) {
				if(entry.getValue() > current.getValue() ) {
					Entry ne = next(current);
					current = ne;
					System.out.println("New element has more priority");
					entryWithPriority = entry;
					
				}else {
					return current;
					
				}
		}
		}else {
			if(entry.getValue() > current.getValue() ) {
				Entry ne = next(current);
				current = ne;
				System.out.println("New element has more priority");
				entryWithPriority = entry;
			}else {
				return current;
				
			}
		}
		return entryWithPriority;
	}
	
	private Entry findLessPriorityElement(Entry entry) {
		Entry current = this.getHead();
		Entry entryWithLessPriority = null;
		if(hasNext(current)) {
		while(hasNext(current)) {
				if(entry.getValue() < current.getValue()) {
					Entry ne = next(current);
					current = ne;
					System.out.println("New element has less priority");
					entryWithLessPriority = entry;
					
				}else {
					return current;
					
				}
		}
		}
			
		if(entry.getValue() < current.getValue()) {
				Entry ne = next(current);
				current = ne;
				System.out.println("New element has less priority");
				entryWithLessPriority = entry;
				
			}else {
				return current;
				
			}
		
		return entryWithLessPriority;
	}
	
	
	
	

	private void delete(Entry entry) {
		if(entry.equals(this.tail) && entry != this.head) {
	     int ind = tail.getIndex() - 1;
		 Entry priviouslyE = getByIndex(ind);
		 priviouslyE.setNextObject(null);
		 this.tail = priviouslyE;
		
		} else if(entry != this.tail && entry.equals(this.head)){
			
			this.head = getByIndex(this.getHead().getIndex() + 1);
			updateIndex();
		} else {
			
		Entry priviosly =	getByIndex(entry.getIndex() - 1);
		priviosly.setNextObject(getByIndex(entry.getIndex() + 1));
		updateIndex();
		
		}
	}
	
	public void deleteByIndex(int index) {
	  Entry entry = getByIndex(index);
	  delete(entry);
	}
	
	
	private void updateIndex() {
		Entry current = this.getHead();
		while(hasNext(current)){
			Entry priviosly = current;
				if(hasNext(current)) {
					Entry next = next(current);
					next.setIndex(priviosly.getIndex() + 1);
					Entry ne = next(current);
					current = ne;
				}else {
					break;
				}
				
			} 
		}
		
	public Entry peek() {
		return this.getHead();
	}
	
	public Entry poll() {
		Entry head = this.getHead();
		deleteByIndex(this.getHead().getIndex());
		return head;
	}
	
	
	
	
	
	public List<Integer> printValues() {
		List<Integer> values = new ArrayList<>();
		Entry current = this.getHead();
		values.add(current.getValue());
		while(hasNext(current)) {
				Entry ne = next(current);
				current = ne;
				values.add(current.getValue());
		}
		return values;
	}
	
	public int getCount() {
		return index;
	}
	
	
	
	public boolean hasNext(Entry entry) {
		if(entry.getNextObject() != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public Entry next(Entry entry) {
		Entry obj = null;
		if(hasNext(entry)) {
			obj = entry.getNextObject();
		}
		return obj;
	}
	
	public static int getIndex() {
		return index;
	}

	public static void setIndex(int index) {
		ProorityQueue.index = index;
	}

	public Entry getHead() {
		return head;
	}

	

	public void setHead(Entry head) {
		this.head = head;
	}
	
	

	public Entry getTail() {
		return tail;
	}

	public void setTail(Entry tail) {
		this.tail = tail;
	}
	
	@Override
	public String toString() {
		return "ProorityQueue [head=" + head + ", tail=" + tail + "]";
	}
	

}
