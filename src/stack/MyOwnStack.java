package stack;

import oneDimensionalArray.Entry;
import oneDimensionalArray.OneDimensionalArray;

public class MyOwnStack {
  
	OneDimensionalArray my = new OneDimensionalArray();
  
  
  public void push(String value) {
	  my.insert(value);
  }
  
  public String pop() {
	  Entry last = my.getTail();
	  my.deleteByIndex(my.getTail().getIndex());
	  return last.getValue();
  }
  
  public String peek() {
	  Entry last = my.getTail();
	  return last.getValue();
  }
  
  public int count() {
	 return my.getCount();
	 }
}
