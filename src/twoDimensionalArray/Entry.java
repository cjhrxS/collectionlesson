package twoDimensionalArray;

public class Entry {

	private String value;
	private Entry privioslyObject;
	private Entry nextObject;
	private int index;	

	public Entry() {
		
	}

	public String getObject() {
		return value;
	}

	public void setObject(String object) {
		this.value = object;
	}

	public Entry getPrivioslyObject() {
		return privioslyObject;
	}

	public void setPrivioslyObject(Entry privioslyObject) {
		this.privioslyObject = privioslyObject;
	}

	public Entry getNextObject() {
		return nextObject;
	}

	public void setNextObject(Entry nextObject) {
		this.nextObject = nextObject;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return "Entry [value=" + value + ", privioslyObject=" + privioslyObject
			+ ", index=" + index + "]";
	}

	
	
	
}
