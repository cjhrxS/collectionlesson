package twoDimensionalArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TwoDimansionalArray {
	
	
	private Entry head;
	private Entry tail;
	private static int index;

	public TwoDimansionalArray() {
		this.head = null;
		this.tail = null;
		this.index = 0;
	}

	public void insert(String value) {
		Entry last = this.getTail();
		if(head == null) {
			Entry e = new Entry();
			e.setObject(value);
			e.setIndex(++index);
			head = e;
		} else {
			Entry newEntry = new Entry();
			newEntry.setObject(value);
			newEntry.setIndex(++index);
			
			Entry priviosly = getByIndex(newEntry.getIndex() - 1);
			priviosly.setNextObject(newEntry);
			
			newEntry.setPrivioslyObject(priviosly);
			
		    tail = newEntry;
			
			
		}
	}

	public Entry getByIndex(int value) {
		Entry current = this.getHead();
	while(hasNext(current)) {
			Entry ne = next(current);
			current = ne;
			if(current.getIndex() == value) {
				return current;
			}
	
	}
		return current;
	}
	
	
	
	private void delete(Entry entry) {
		if(entry.equals(this.tail) && entry != this.head) {
			
		 Entry priviouslyE = getByIndex(tail.getIndex()-1);
		 priviouslyE.setNextObject(null);
		 this.tail = priviouslyE;
		
		} else if(entry != this.tail && entry.equals(this.head)){
			
			this.head = getByIndex(this.getHead().getIndex() + 1);
			updateIndex();
		} else {
			
		Entry priviosly =	getByIndex(entry.getIndex() - 1);
		priviosly.setNextObject(getByIndex(entry.getIndex() + 1));
		
		Entry next = getByIndex(entry.getIndex() + 1);
		next.setPrivioslyObject(priviosly);
		updateIndex();
		
		}
	}
	
	public void deleteByIndex(int index) {
	  Entry entry = getByIndex(index);
	  delete(entry);
	}
	
	
	private void updateIndex() {
		Entry current = this.getHead();
		for (int i = 0; i < this.tail.getIndex(); i++) {
			Entry priviosly = current;
				if(hasNext(current)) {
					Entry next = next(current);
					next.setIndex(priviosly.getIndex() + 1);
					Entry ne = next(current);
					current = ne;
				}else {
					break;
				}
				
			} 
		}
		
	
	
	
	public List<String> printValues() {
		List<String> values = new ArrayList<>();
		Entry current = this.getHead();
		while(hasNext(current)) {
				Entry ne = next(current);
				current = ne;
		         values.add(current.getObject());
		
		}
		return values;
	}
	
	public boolean hasNext(Entry entry) {
		if(entry.getNextObject() != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public Entry next(Entry entry) {
		Entry obj = null;
		if(hasNext(entry)) {
			obj = entry.getNextObject();
		}
		return obj;
	}

	public Entry getHead() {
		return head;
	}

	public void setHead(Entry head) {
		this.head = head;
	}

	public Entry getTail() {
		return tail;
	}

	public void setTail(Entry tail) {
		this.tail = tail;
	}

	

	@Override
	public String toString() {
		return "LinkedList [head=" + head + ", tail=" + tail + "]";
	}
	

	
	

	
	

}
